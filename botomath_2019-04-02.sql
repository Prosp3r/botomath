
# Dump of table bots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bots`;

CREATE TABLE `bots` (
  `botid` int(11) NOT NULL AUTO_INCREMENT,
  `botname` varchar(100) DEFAULT NULL,
  `bottype` varchar(20) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `timecreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`botid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table botskillset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `botskillset`;

CREATE TABLE `botskillset` (
  `entryid` int(11) NOT NULL AUTO_INCREMENT,
  `botid` int(11) DEFAULT NULL,
  `taskid` int(11) DEFAULT NULL,
  PRIMARY KEY (`entryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table bottasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bottasks`;

CREATE TABLE `bottasks` (
  `jobid` int(11) NOT NULL AUTO_INCREMENT,
  `taskid` int(11) DEFAULT NULL,
  `botid` int(11) DEFAULT NULL,
  `taskdescription` text DEFAULT NULL,
  `tasketa` int(11) DEFAULT NULL,
  `taskscore` int(11) DEFAULT NULL,
  `starttime` datetime(6) NOT NULL,
  `endtime` datetime(6) DEFAULT NULL,
  `taskstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`jobid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table tasktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tasktypes`;

CREATE TABLE `tasktypes` (
  `taskid` int(11) NOT NULL AUTO_INCREMENT,
  `taskdescription` text DEFAULT NULL,
  `tasketa` decimal(14,2) DEFAULT NULL,
  `taskscore` decimal(14,2) DEFAULT NULL,
  `taskstatus` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `tasktypes` WRITE;
/*!40000 ALTER TABLE `tasktypes` DISABLE KEYS */;

INSERT INTO `tasktypes` (`taskid`, `taskdescription`, `tasketa`, `taskscore`, `taskstatus`)
VALUES
	(1,'do the dishes',1000.00,100.00,'a'),
	(2,'sweep the house',3000.00,300.00,'a'),
	(3,'do the laundry',10000.00,1000.00,'a'),
	(4,'take out the recycling',4000.00,400.00,'a'),
	(5,'make a sammich',7000.00,700.00,'a'),
	(6,'mow the lawn',20000.00,2000.00,'a'),
	(7,'rake the leaves',18000.00,1800.00,'a'),
	(8,'give the dog a bath',14500.00,1450.00,'a'),
	(9,'bake some cookies',8000.00,800.00,'a'),
	(10,'wash the car',20000.00,2000.00,'a');

/*!40000 ALTER TABLE `tasktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `timejoined` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
