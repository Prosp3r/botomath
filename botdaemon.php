<?php

#if(!class_exists('../classes/main.php')){ include_once('../classes/main.php'); }
include_once('/var/www/html/classes/conf.php');


#Returns all variables defined for operations
   /**all the definitions of variables and access them by calling this function*/
    function variables(){
  
        #:::::::::::::::
        #:::OPS VARS::::
        #:::::::::::::::
         
         #FOR LIVE OPERATIONS
        $jobTable = "bottasks";
        $users_table = "users";
        $tasktypesTable = "tasktypes";
        $botskillsTable = "botskillset";
        $botsTable = "bots";
        
        
        $variablex = array(
            'users_table' => $users_table,
            'jobTable' => $jobTable,
            'usersTable'=>$users_table,
            'tasktypesTable'=>$tasktypesTable,
            'botskillsTable'=>$botskillsTable,
            'botsTable'=>$botsTable
        );
        
        $tosend = json_encode($variablex);
        $tosent = json_decode($tosend);
        return $tosent;
    }
    
/***/    
function _straightQuery($Query){
    $dbcon = pdo_dbcon();
    $stmt = $dbcon->query($Query);
    #return $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $stmt;
}

#returns number of affected rows
function _numQuery($Query){
    $dbcon = pdo_dbcon();
    $stmt = $dbcon->exec($Query);
    #return $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $stmt;
}




/**
* microTimeDifference returns a positive difference between time times
* @param time $timeOne Complete dates to micro seconds e.g. YYYY-MM-DD H:i:s.123456 
* @param time $timeTwo Complete dates to micro seconds e.g. YYYY-MM-DD H:i:s.123457
* @return int microtime value from the difference e.g. 123451
*/
function microTimeDifference( $timeOne, $timeTwo){
   $microTimeOne = returnMicroSeconds( $timeOne );
   $microTimeTwo = returnMicroSeconds( $timeTwo );
   $difference = abs( $microTimeOne-$microTimeTwo );
   return $difference;
}

/**
* returnMicroSeconds takes a complete datetime(6) value and returns the microsecond portion of it.
* @param datetime(6)
* @return int $microsec
*/
function returnMicroSeconds( $timeValue ){
   $timex = explode('.', $timeValue);
   $microSec = $timex[1];
   return $microSec;
}

/**
* returnDateTime
*/
function returnDateTime( $timeValue ){
   $timex = explode('.', $timeValue);
   $mainTime = $timex[0];
   return $mainTime;
}

/**
* addMicroSecondsToTime 
*/
function addMicroSecondsToTime( $timeValue, $microSec ){
   $fmicrosec = $this->returnMicroSeconds( $timeValue );
   $totalMS = $fmicrosec + $microSec;
   $mainTime = $this->returnDateTime( $timeValue );
   return "$mainTime.$totalMS";
}

/**
* milliToMicroSeconds will convert a millisecond value to microsecond
* @param int milliseconds
* @return int microseconds converted value
*/
function milliToMicroSeconds( $milliSecs ){
   return round($milliSecs / 1000);
}

/**
* microToMilliSeconds will convert a microsecond value to millisecond
* @param int microSecs
* @return int milliseconds converted value
*/
function microToMilliSeconds( $microSecs ){
   return round($microSecs * 1000);
}


/**
 * getIncompleteBotJobs returns one incomplete job at a time from the job database
 */
function getIncompleteBotJobs(){
    $thisx = variables();
    $Query = "SELECT * FROM $thisx->jobTable WHERE taskstatus = '0' ORDER BY starttime LIMIT 1 ";
    $fetchJobs = _straightQuery($Query);
    $row = $fetchJobs->fetch(PDO::FETCH_ASSOC);
    return $row;
}

/**
 * flags a job
 * 
 * @param type $jobId
 * @param type $flag
 * @return type
 */
function changeJobStatus($jobId, $flag){
    $thisx = variables();
    $Query = "UPDATE $thisx->jobTable SET taskstatus = '$flag'  WHERE jobid = '$jobId' ";
    $result = _numQuery($Query);
    return $result;
}

/**
 * doJobs is the actual robot helper that does the jobs
 */
function doJobs(){
    $nextJob = getIncompleteBotJobs();
    $jobId              = $nextJob['jobid'];
    $taskid             = $nextJob['taskid'];
    $botid              = $nextJob['botid'];
    $taskdescription    = $nextJob['taskdescription'];
    $tasketa            = milliToMicroSeconds($nextJob['tasketa']);//milli seconds to Micro-Seconds
    $taskstatus         = $nextJob['taskstatus'];
    $starttime          = $nextJob['starttime'];
    $endtime            = $nextJob['endtime'];
    $taskscore          = $nextJob['taskscore'];
    
    $microNow = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
    $thisTime = $microNow->format("Y-m-d H:i:s.u");
    //CALCULATE TIME DIFFERENCE
    
    //Get Microtime for start time
    $timexOne = explode('.', $starttime);
    $microSecOne = $timexOne[1];
    
    //Get Microtime for this time
    $timexTwo = explode('.', $thisTime);
    $microSecTwo = $timexTwo[1];
    
    $timeDiff = abs( $microSecOne-$microSecTwo );
    
    if($timeDiff >= $tasketa){
        //TASK TIME HAS ELASPED
        $flag = 1;
        changeJobStatus($jobId, $flag);
    }
}


// The worker will execute every X seconds:
$seconds = 1;
// We work out the micro seconds ready to be used by the 'usleep' function.
$micro = $seconds * 1000000;

while(true){
 // This is the code you want to loop during the service...
 doJobs();
 // Now before we 'cycle' again, we'll sleep for a bit...
 usleep($micro);
}
